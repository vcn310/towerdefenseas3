package  
{
	import com.telosinternational.starlingbasic.Broadcaster;
	import starling.display.Image;
	import starling.events.*;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author nguyen
	 */
	public class Bullet extends starling.display.Sprite
	{
		
		private var _target:Enemy;
		private var _damage:int;
 
		private var xSpeed:Number;
		private var ySpeed:Number;
		private var maxSpeed:Number = 500;
		
		private var xoff:Number = 10;
		private var yoff:Number = 8;;
		public function Bullet() 
		{		
			var rect:Sprite = new flash.display.Sprite();
			rect.graphics.beginFill(0x00FF00, 1);
			rect.graphics.drawCircle( Game.SQUARE_SIZE/8, Game.SQUARE_SIZE/8, Game.SQUARE_SIZE/8);
			rect.graphics.endFill();
				
			var bmd:BitmapData = new BitmapData(rect.width,rect.height, true, 0x00000000);
			bmd.draw(rect);
			var _img:Image =  new Image(Texture.fromBitmapData(bmd, false, false));
			
			addChild(_img);
		}
		
		public function eFrame(e:EnterFrameEvent):void 
		{
			var delta:Number = e.passedTime;
			var yDist:Number=-yoff+target.y+ Game.SQUARE_SIZE/2 - this.y;//how far this guy is from the enemy (x)
			var xDist:Number=-xoff+target.x+ Game.SQUARE_SIZE/2 - this.x;//how far it is from the enemy (y)
			var angle:Number=Math.atan2(yDist,xDist);//the angle that it must move
			ySpeed=Math.sin(angle) * maxSpeed;//calculate how much it should move the enemy vertically
			xSpeed=Math.cos(angle) * maxSpeed;//calculate how much it should move the enemy horizontally
			//move the bullet towards the enemy
			this.x+= xSpeed*delta;
			this.y += ySpeed * delta;
			
			if (xSpeed == 0 || ySpeed == 0)
				trace("??");
 
			if(this.bounds.intersects(target.bounds)){//if it touches the enemy
				target.health -= damage;//make it lose some health
				//destroyThis();
				this.removeEventListeners();
				Broadcaster.instance.appBroadcast(Level.REMOVE_BULLET, [this]);
			}
			if (target == null )
			{
				//destroyThis();
				this.removeEventListeners();
				Broadcaster.instance.appBroadcast(Level.REMOVE_BULLET, [this]);
			}
		}
		
		public function destroyThis():void
		{
			this.removeEventListener(Event.ENTER_FRAME, eFrame);
			removeEventListeners();
			Broadcaster.instance.appBroadcast( Turret.REMOVE_BULLET,[this]);
		}
		
		
		public function get damage():int 
		{
			return _damage;
		}
		
		public function set damage(value:int):void 
		{
			_damage = value;
		}
		
		public function get target():Enemy 
		{
			return _target;
		}
		
		public function set target(value:Enemy):void 
		{
			_target = value;
		}
		
	}

}