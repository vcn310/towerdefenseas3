package{
	//imports
	import com.telosinternational.starlingbasic.StarlingUtils;
	
	import starling.events.*;

	import starling.display.Image;
	
	public class DirectBlock {
		
		private var _root:Image;
		private var _directType:String;
 
		
		public function DirectBlock(type:int, xVal:int, yVal:int) {
			switch (type)
			{
				case 2:
					directType = Level.DOWN;
					break;
				case 8:
					directType = Level.UP;
					break;
				case 4:
					directType = Level.LEFT;
					break;
				case 6:
					directType = Level.RIGHT;
					break;
				case 3:
					directType = Level.START;
					break;
				case 9:
					directType = Level.STOP;
					break;
				default:
					directType = "NO";
			}
			
			_root = StarlingUtils.createRectImage( Game.SQUARE_SIZE , Game.SQUARE_SIZE, 0x111111, 1 );
			
			//this._root.addEventListener(Event.ENTER_FRAME, eFrame);
 
		
			this._root.x = xVal;
			this._root.y = yVal;
		}
		
		
		private function eFrame(e:Event):void{
		
		}
		
		public function get root():Image 
		{
			return _root;
		}
		
		public function set root(value:Image):void 
		{
			_root = value;
		}
		
		public function get directType():String 
		{
			return _directType;
		}
		
		public function set directType(value:String):void 
		{
			_directType = value;
		}
	}
}