package  
{
	import Turret;
	import com.telosinternational.starlingbasic.StarlingUtils;
	import flash.geom.Point;
	import starling.display.Image;
	import starling.display.DisplayObject;
	import starling.textures.Texture;
	import starling.events.*;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	
	public class EmptyBlock {//defining the class as EmptyBlock
		private var _root:Image;
		private var _temp:Image;
		private var circleRange:Image;
		private var isHover:Boolean = false;
 
		public function EmptyBlock(xPos:Number, yPos:Number) {
			
			_root = StarlingUtils.createRectImage( Game.SQUARE_SIZE, Game.SQUARE_SIZE, 0x555555, 1 );
			_root.x = xPos;
			_root.y = yPos;
			_temp = StarlingUtils.createRectImage( Game.SQUARE_SIZE, Game.SQUARE_SIZE, 0x61E873, 0.8 );
			var rect:Sprite = new flash.display.Sprite();
			rect.graphics.beginFill(0x61E873, 0.3);
			rect.graphics.drawCircle(Turret.RANGE, Turret.RANGE, Turret.RANGE);
			rect.graphics.endFill();
			var bmd:BitmapData = new BitmapData(rect.width, rect.height, true, 0x00000000);
			bmd.draw(rect);
			circleRange =  new Image(Texture.fromBitmapData(bmd, false, false));
			
			circleRange.x = xPos - Turret.RANGE + Game.SQUARE_SIZE/2;
			circleRange.y = yPos - Turret.RANGE + Game.SQUARE_SIZE/2;
			//circleRange.pivotX = Game.SQUARE_SIZE /2 ;
			//circleRange.pivotY = Game.SQUARE_SIZE/2 ;
			
			
			//this._root.addEventListener(Event.ENTER_FRAME, eFrameEvents);
			this._root.addEventListener(TouchEvent.TOUCH, onTouch);
			this._temp.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private function onTouch(e:TouchEvent):void 
		{
			var touchStart:Touch = e.getTouch( _root, TouchPhase.BEGAN );
			var touchMove:Touch = e.getTouch( _root, TouchPhase.MOVED );
			var touchEnd:Touch = e.getTouch( _root, TouchPhase.ENDED );
		    var touchOver:Touch = e.getTouch(_root, TouchPhase.HOVER);
			
			if ( touchStart)
			{
				var turrent:Turret = new Turret(_root.x, _root.y);
				(_root.parent).addChild(turrent);
			}
			
			
			var isOver:Touch = e.getTouch(e.target as DisplayObject, TouchPhase.HOVER);
			if (isOver)
			{
				(_root.parent).addChild(_temp);
				_temp.x = _root.x;
				_temp.y = _root.y;	
				(_root.parent).addChild(circleRange);
			}
			else if (!isHover)
			{
				(_root.parent).removeChild(_temp);
				isHover = false;
				(_root.parent).removeChild(circleRange);
			}
		}
		
		
		private function eFrameEvents(e:Event):void{
			//
		}
		
		
		public function get root():Image 
		{
			return _root;
		}
		
		public function set root(value:Image):void 
		{
			_root = value;
		}
	}

}