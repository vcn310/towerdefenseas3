package  
{
	import com.telosinternational.starlingbasic.Broadcaster;
	import flash.display.Shape;
	import flash.geom.Point;
	import starling.display.Image;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import starling.textures.Texture;
	import starling.events.*;
	/**
	 * ...
	 * @author nguyen
	 */
	public class Enemy extends starling.display.Sprite
	{
		private var _img:Image;
		private var _speed:Number = 60;
		private var _xSpeed:Number;
		private var _ySpeed:Number;
		private var _direct:String;
		private var _health:int = 200;
		
		public function Enemy(pos:Point, dir:String) 
		{
			var rect:Sprite = new flash.display.Sprite();
		
			rect.graphics.beginFill(0xFF0000, 1);
			rect.graphics.drawCircle( Game.SQUARE_SIZE/3.2, Game.SQUARE_SIZE/3.2, Game.SQUARE_SIZE/3.0);
			//var array:Vector.<Number> = new Vector.<Number>();
			//array.push(Game.SQUARE_SIZE / 2);
			//array.push(0);
			//array.push(Game.SQUARE_SIZE / 2);
			//array.push(Game.SQUARE_SIZE / 2);
			//array.push(0);
			//array.push(Game.SQUARE_SIZE / 2);
			//
			//rect.graphics.drawTriangles(array);
			rect.graphics.endFill();
				
			var bmd:BitmapData = new BitmapData(rect.width,rect.height, true, 0x00000000);
			bmd.draw(rect);
			_img =  new Image(Texture.fromBitmapData(bmd, false, false));
			addChild(_img);
			
			///this.x = pos.x - Game.SQUARE_SIZE / 2;
			//this.y = pos.y - Game.SQUARE_SIZE / 2;
			
			this.x = pos.x + Game.SQUARE_SIZE/4;
			this.y = pos.y + Game.SQUARE_SIZE / 4;
			
			speed = ((Math.random() * 100) % 30) + 50;
			turn(dir);
			
			this.addEventListener(Event.ENTER_FRAME, eFrame);
		}
		
		private function eFrame(e:EnterFrameEvent):void
		{
			var delta:Number = e.passedTime;
			this.x += xSpeed*delta;
			this.y += ySpeed*delta;
			
			if (health <= 0)
			{
				destroyThis();
			}
		}
		
		public function turn(dir:String):void
		{
			if(dir == Level.UP){
				this.xSpeed = 0;
				this.ySpeed = -speed;
			} 
			else if(dir == Level.RIGHT)
			{
				this.xSpeed = speed;
				this.ySpeed = 0;
			} 
			else if (dir == Level.DOWN)
			{
				this.xSpeed = 0;
				this.ySpeed = speed;
			} 
			else if (dir == Level.LEFT)
			{
				this.xSpeed = -speed;
				this.ySpeed = 0;
			}
			else if (dir == Level.STOP)
			{
				destroyThis();		
			}
			else if (dir == "STOP")
			{
				this.xSpeed = 0;
				this.ySpeed = 0;
			}
			if (dir != Level.START)
				direct = dir;
		}
			
		public function destroyThis():void
		{
			this.removeEventListener(Event.ENTER_FRAME, eFrame);
			removeEventListeners();
			//removeChild(_img);
			//_img.dispose();
			//_img = null;
			Broadcaster.instance.appBroadcast( Level.REMOVE_ENEMY,[this]);
		}
		
		public function set img(value:Image):void 
		{
			_img = value;
		}
		
		public function get speed():Number 
		{
			return _speed;
		}
		
		public function set speed(value:Number):void 
		{
			_speed = value;
		}
		
		public function get xSpeed():Number 
		{
			return _xSpeed;
		}
		
		public function set xSpeed(value:Number):void 
		{
			_xSpeed = value;
		}
		
		public function get ySpeed():Number 
		{
			return _ySpeed;
		}
		
		public function set ySpeed(value:Number):void 
		{
			_ySpeed = value;
		}
		
		public function get direct():String 
		{
			return _direct;
		}
		
		public function set direct(value:String):void 
		{
			_direct = value;
		}
		
		public function get health():int 
		{
			return _health;
		}
		
		public function set health(value:int):void 
		{
			_health = value;
		}
		
	}

}