package  
{
	import com.telosinternational.starlingbasic.StarlingUtils;
	import DirectBlock;
	import EmptyBlock;
	import Level;
	import flash.display.Shape;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	
	
	public class Game extends Sprite
	{
		static public const SQUARE_SIZE:Number = 25;
		static public const PI:Number = 3.141592653589793238;
		
		private var directBlocks:Vector.<DirectBlock>;
		
		public function Game() 
		{	
			directBlocks = new Vector.<DirectBlock>();

			var gameOver:Boolean = false;
			
			var lvl:Level = new Level();
			var lvlArray:Array = lvl.lvlArray;
			
			var roadHolder:Sprite = new Sprite();//create an object that will hold all parts of the road
			addChild(roadHolder);//add it to the stage
						
				var row:int = 0;//the current row we're working on
				for (var i:int = 0; i < lvlArray.length; i++)
				{
					if(lvlArray[i] == 0){
						var block:EmptyBlock = new EmptyBlock((i-row*22)*SQUARE_SIZE, row*SQUARE_SIZE);
						addChild(block.root);				
					} 
					else if(lvlArray[i] == 1){				
						var img:Image = StarlingUtils.createRectImage( SQUARE_SIZE, SQUARE_SIZE, 0x111111, 1 );					
						img.x= (i-row*22)*SQUARE_SIZE;
						img.y = row*SQUARE_SIZE;
						roadHolder.addChild(img);
					} 
					else 
					{
						var block2:DirectBlock  = new DirectBlock(lvlArray[i],(i-row*22)*SQUARE_SIZE,row*SQUARE_SIZE);
						addChild(block2.root);
						directBlocks.push(block2);
					}
					
					for(var c:int = 1;c<=16;c++){
						if(i == c*22-1){
							//if 22 columns have gone by, then we move onto the next row
							row++;
						}
					}
					
				}
			lvl.directBlocks = directBlocks;
			addChild(lvl);
		}	
		
	
		
		private function startGame():void {
		
		}

	}

}