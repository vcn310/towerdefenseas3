package  
{
	import com.telosinternational.starlingbasic.Broadcaster;
	import flash.geom.Point;
	import starling.display.Sprite;
	import starling.events.*;
	/**
	 * ...
	 * @author nguyen
	 */
	public class Level extends Sprite
	{
		static public const GET_ENEMY:String = "get_enemy";
		static public const REMOVE_ENEMY:String = "remove_enemy";
		
		static public const GET_BULLET:String = "get_bullet";
		static public const REMOVE_BULLET:String = "remove_bullet";
		
		static public const UP:String = "UP";
		static public const DOWN:String = "DOWN";
		static public const LEFT:String = "LEFT";
		static public const RIGHT:String = "RIGHT";
		static public const START:String = "START";
		static public const STOP:String = "STOP";
		
		static public const START_X:Number = 0;//21 * Game.SQUARE_SIZE;
		static public const START_Y:Number = 5 * Game.SQUARE_SIZE;
		
		private var _lvlArray:Array;
		private var _directBlocks:Vector.<DirectBlock>;
		
		private var  _enemies:Vector.<Enemy>;
		private var _waves:Array;	
		private var currentWave:int = 0;
		private var currentEnemy:int = 0;//the current enemy that we're creating from the array
		private var enemyTime:int = 0;//how many frames have elapsed since the last enemy was created
		private var enemyLimit:int = 20;//how many frames are allowed before another enemy is created
		private var enemiesLeft:int;//how many enemies are left on the field

		private var startDir:String;
		private var finDir:String;
		
		private var numBullet:int = 0;
		private var activeBullets:Vector.<Bullet>;
		private var inactiveBullets:Vector.<Bullet>;
		
		public function Level() 
		{
			lvlArray = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
						0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
						0,0,0,0,6,1,1,2,0,0,6,1,1,2,0,0,6,1,1,2,0,0,
						0,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,
						0,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,
						3,2,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,6,1,9,
						0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,0,0,0,
						0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,0,0,0,
						0,6,1,1,8,0,0,6,1,1,8,0,0,6,1,1,8,0,0,0,0,0,
						0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
						0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
						0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
						];
						
			//lvlArray = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
						//0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
						//0,0,0,0,2,1,1,4,0,0,2,1,1,4,0,0,2,1,1,4,0,0,
						//0,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,
						//0,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,
						//3,4,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,8,1,9,
						//0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,0,0,0,
						//0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,0,0,0,
						//0,8,1,1,4,0,0,8,1,1,4,0,0,8,1,1,4,0,0,0,0,0,
						//0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
						//0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
						//0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
						//];
			
			startDir = Level.RIGHT;
			finDir = Level.RIGHT;
			
			enemies = new Vector.<Enemy>();
			waves = [
						[1],
						[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
						[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
						];
					
			activeBullets = new Vector.<Bullet>();
			inactiveBullets = new Vector.<Bullet>();
			
			this.addEventListener(Event.ENTER_FRAME, eFrame);
			Broadcaster.instance.addAppListener( GET_ENEMY, this, getEnemies );
			Broadcaster.instance.addAppListener( REMOVE_ENEMY, this, removeEnemy );
			
			Broadcaster.instance.addAppListener( GET_BULLET, this, getBullet );
			Broadcaster.instance.addAppListener( REMOVE_BULLET, this, removeBullet );
		}
		
		private function getBullet():Bullet
		{
			var newBullet:Bullet;
			if (inactiveBullets.length == 0)
			{
				newBullet = new Bullet();
				
			}
			else
			{
				newBullet = inactiveBullets.pop();
			}
			activeBullets.push(newBullet);
			//newBullet.x = xPos - 3;
			//newBullet.y = yPos - 3;;
			//newBullet.target = target;
			//newBullet.damage = dmg;
			
			numBullet++;
			return newBullet;
		}
		
		private function removeBullet(b:Bullet):void
		{
			
			(this.parent).removeChild(b);
			numBullet --;
			var i:int = activeBullets.indexOf(b);
			
			
			activeBullets.splice(i, 1);
			inactiveBullets.push(b);
		}
		
		private function eFrame(e:Event):void
		{
			if(enemyTime < enemyLimit){//if it isn't time to make them yet
				enemyTime ++;//then keep on waiting
			} 
			else 
			{
				var theCode:int =  1;// waves[currentWave][currentEnemy];
				if (theCode == 1)
				{
					var newEnemy:Enemy = new Enemy(new Point(START_X,START_Y), startDir);
					addChild(newEnemy);
					enemies.push(newEnemy);
				}
				currentEnemy ++;//move on to the next enemy
				enemyTime = 0;//and reset the time
			}	
			
			for (var i:int = 0; i < enemies.length; i++)
				for (var j:int = 0; j < directBlocks.length; j++)
				{
					var enemy:Enemy = enemies[i];
					var block:DirectBlock = directBlocks[j];
					if (checkInside(enemy, block))
					{
						if (block.directType != "NO")
						//	enemy.turn("STOP");
							enemy.turn(block.directType);
					}
				}
			
			//trace("Active="+activeBullets.length+ "    " + "Inactive="+inactiveBullets.length)+"       num="+numBullet.toString();
		}
		
		private function checkInside(enemy:Enemy, block:DirectBlock):Boolean
		{
			var xPos:Boolean = false;
			var yPos:Boolean = false;
		
			if (Math.abs(block.root.x - enemy.x+5 ) < 4)
					xPos = true;
							
			if (enemy.direct == Level.RIGHT || enemy.direct == Level.LEFT )
			{		
				if (Math.abs(enemy.y - block.root.y) < 7)
					yPos = true;
			}
			if (enemy.direct == Level.DOWN)
			{
				if ((block.root.y - enemy.y ) < -3)
					yPos = true;
			}
			if (enemy.direct == Level.UP)
			{
				if ((enemy.y - block.root.y ) < 7)
					yPos = true;
			}
			
				
			return xPos && yPos;
		}
		
		private function removeEnemy(e:Enemy):void 
		{
			enemies.splice(enemies.indexOf(e), 1);
			removeChild(e);
			e.dispose();
			e = null;
		}
		
		private function getEnemies():Vector.<Enemy>
		{
			return _enemies;
		}
		
		public function get lvlArray():Array 
		{
			return _lvlArray;
		}
		
		public function set lvlArray(value:Array):void 
		{
			_lvlArray = value;
		}
		
		public function get directBlocks():Vector.<DirectBlock> 
		{
			return _directBlocks;
		}
		
		public function set directBlocks(value:Vector.<DirectBlock>):void 
		{
			_directBlocks = value;
		}
		
		public function get enemies():Vector.<Enemy> 
		{
			return _enemies;
		}
		
		public function set enemies(value:Vector.<Enemy>):void 
		{
			_enemies = value;
		}
		
		public function get waves():Array 
		{
			return _waves;
		}
		
		public function set waves(value:Array):void 
		{
			_waves = value;
		}
		
		
		
	}

}