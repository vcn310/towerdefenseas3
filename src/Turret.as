package  
{
	import starling.display.Image;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import com.telosinternational.starlingbasic.StarlingUtils;
	import com.telosinternational.starlingbasic.Broadcaster;
	import starling.textures.Texture;
	import starling.events.*;

	public class Turret extends starling.display.Sprite
	{	
		static public const REMOVE_BULLET:String = "remove_bullet";
		
		static public const RANGE:Number = 150;
		
		private var _img:Image;
		
		private var angle:Number; //the angle that the turret is currently rotated at
		private var radiansToDegrees:Number = 180/Math.PI;
		private var damage:int = 10;
		private var range:int = RANGE;
		private var enTarget:Enemy;
		private var cTime:Number = 0;//how much time since a shot was fired by this turret
		private var reloadTime:Number = 0.1;//how long it takes to fire another shot
		private var loaded:Boolean = true;//whether or not this turret can shoot

		private var xoff:Number = 10;
		private var yoff:Number = 8;;

		
		
		public function Turret (x:Number, y:Number) 
		{
			
			var rect:Sprite = new flash.display.Sprite();
			rect.graphics.beginFill(0x61E873, 0.8);
			rect.graphics.drawCircle(Game.SQUARE_SIZE/2, Game.SQUARE_SIZE/2, Game.SQUARE_SIZE/2);
			rect.graphics.endFill();
			rect.graphics.beginFill(0xFFFFFF);
			rect.graphics.drawRect(Game.SQUARE_SIZE/2 -2.5, Game.SQUARE_SIZE/2, 5, 20);
			rect.graphics.endFill();
			var bmd:BitmapData = new BitmapData(rect.width, rect.height, true, 0x00000000);
			bmd.draw(rect);
			_img =  new Image(Texture.fromBitmapData(bmd, false, false));
			
			addChild(_img);
			this.x = x + Game.SQUARE_SIZE /2;
			this.y = y + Game.SQUARE_SIZE /2;
			this.pivotX = Game.SQUARE_SIZE /2 ;
			this.pivotY = Game.SQUARE_SIZE/2 ;
			
			
		
			this.addEventListener(Event.ENTER_FRAME, eFrame);
			Broadcaster.instance.addAppListener( REMOVE_BULLET, this, removeBullet );
		}
		
		private function removeBullet(b:Bullet):void
		{
			(this.parent).removeChild(b);
			b.dispose();
			b = null;
		}
		
		private function eFrame(e:EnterFrameEvent):void 
		{
			var delta:Number = e.passedTime;
			
			var enemies:Vector.<Enemy> = Broadcaster.instance.appBroadcast( Level.GET_ENEMY,[]);
			//FINDING THE NEAREST ENEMY WITHIN RANGE
			var distance:Number = range;
			enTarget = null;
			for(var i:int=enemies.length-1;i>=0;i--){
				var cEnemy:Enemy = enemies[i];
				if(Math.sqrt(Math.pow(cEnemy.y - y, 2) + Math.pow(cEnemy.x - x, 2)) < distance){
					enTarget = cEnemy;
				}
			}
			//ROTATING TOWARDS TARGET
			if (enTarget != null)
			{
				this.rotation = Math.atan2(yoff+enTarget.y-y, xoff+enTarget.x-x) - Math.PI/2 ;
				if(loaded){//if the turret is able to shoot
					loaded = false;//then make in unable to do it for a bit
					var newBullet:Bullet = Broadcaster.instance.appBroadcast(Level.GET_BULLET, []);
					newBullet.x = this.x - 3;
					newBullet.y = this.y - 3;;
					newBullet.target = enTarget;
					newBullet.damage = damage;
					newBullet.addEventListener(Event.ENTER_FRAME, newBullet.eFrame);
					(this.parent).addChild(newBullet);
					
				}
			}
			//LOADING THE TURRET
			if (!loaded)
			{
				cTime += delta;
				if (cTime > reloadTime)
				{
					loaded = true;//load the turret
					cTime = 0;//and reset the time
				}
			}
		}
		
		public function get img():Image 
		{
			return _img;
		}
		
		public function set img(value:Image):void 
		{
			_img = value;
		}
		
		
		
		
		
		
	}

}